This project contains all programming examples of 
Stroustrup's book TCPL 4th Edition.

The names of the .cpp files are named after the name of chapters
in the book. Thus, each .cpp or .h file contains complete 
working source codes of a particular chapter.

Your download directory should look as follows:-
+Stroustrupbook
	-2main.cpp
	-2TheBasics.cpp
	-2TheBasics.h

For bugs and errors, contact me at enthusiast.sahil@yahoo.in.
Happy C++11 coding!
