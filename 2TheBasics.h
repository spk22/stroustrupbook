/**
* Author: Sahil (email: enthusiast.sahil@yahoo.in)
* Date:
* Description: This file contains all programming examples of 2nd Chapter.
*/

#ifndef THE_BASICS_H
#define THE_BASICS_H

void run_TheBasics();
void _initializer_list();   //Section 2.2.2
void _constness();      //Section 2.2.3

/*Vector - full interface, as per chapter 2*/
class Vector        //Section 2.4.3.2
{
public:
    Vector(int);
    double& operator[](int);    //double&, so as to make v[i] an lvalue (assignable)
    int size() {return sz;}
    ~Vector();
private:
    int sz;
    double* elem;
};

void _vectors();
void _static_assertions();

#endif // THE_BASICS_H

