/**
 * Author: Sahil Kakkar <enthusiast.sahil@yahoo.in>
 * Date:
 * Description: This file contains all programming examples of 4th Chapter.
 */

#include <iostream>
#include <string>
#include <cctype>   //contains toupper()

#include "4ContainersAndAlgorithms.h"

std::string compose(const std::string& name, const std::string& domain)
{
    return name + '@' + domain;
    //std::string has a move constructor, so returning long strings by value is efficient.
}

std::string m3(std::string name)
{
    std::string s = name.substr(6, 6);      //s = "Kakkar"
    name.replace(0, 5, "vikrant");      //name = "vikrant Kakkar"
    name[0] = toupper(name[0]);
    return name;
}

void _strings()
{
    auto addr = compose("enthusiast.sahil", "yahoo.in");
    std::cout << addr << '\n';

    std::string name {"Sahil Kakkar"};
    std::cout << m3(name) << '\n';
}

void hello_line()
{
    std::string str;
    std::cout << "Please enter your name: ";
    getline(std::cin, str);
    std::cout << "Hello, " << str << "!\n";
}

std::ostream& operator<<(std::ostream& outs, const Entry& e)
{
    return outs << "{\"" << e.name << "\", " << e.house_number << "}";
}

std::istream& operator>>(std::istream& ins, Entry& e)
{
    char c, c2;
    if(ins >> c && c == '{' && ins >> c2 && c2 == '"')
    {
        std::string name;
        //get(ch) reads one character and stores it to ch if available.
        //Otherwise, leaves ch unmodified and sets failbit and eofbit
        //Note that get(ch) does not skips whitespace, but ins >> ch skips whitespace by default
        while(ins.get(c) && c != '"')
            name += c;

        if(ins >> c && c == ',')
        {
            int number = 0;
            if(ins >> number >> c && c == '}')
            {
                e = {name, number};
                return ins;
            }
        }
    }
    //ins.setf(std::ios_base::failbit);
    return ins;
}

void _streamio()
{
    hello_line();
    Entry neighbour {std::string("Satpaal Madaan"), 218};
    std::cout << neighbour << '\n';
    Entry another;
    std::cout << "Enter another Entry :\n";
    std::cin >> another;
    std::cout << another << '\n';
}

void _containers()
{
    ;
}

void run_ContainersAndAlgorithms()
{
    _strings();
    _streamio();
    _containers();
}
