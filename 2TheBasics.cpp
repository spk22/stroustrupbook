/**
* Author: Sahil (email: enthusiast.sahil@yahoo.in)
* Date:
* Description: This file contains all programming examples of 2nd Chapter.
*/

#include <complex>
#include <vector>
#include <stdexcept>
#include <iostream>
#include "2TheBasics.h"

/*Local functions*/

constexpr double square(double x)   //declared constexpr for call in max1 expression
{
    return x*x;
}

double sum(const std::vector<double>& x)     //sum() will not modify its arguments
{
    double sum {0.0};
    for (auto& elem : x)
        sum += elem;
    return sum;
}

/*Vector - full implementation with exceptions, as per chapter 2*/
Vector::Vector(int n)
{
    if(n <= 0)
        throw std::length_error{"In Vector::Vector(int), number of elements in Vector not a positive value\n"};  //to report non-positive number of elements in vector
    sz = n;
    elem = new double[sz];
}

double& Vector::operator[](int index)
{
    if(index < 0 || index >= size())
        throw std::out_of_range{"In Vector::operator[], index is out of range\n"};
    return elem[index];
}

Vector::~Vector()
{
    delete [] elem;
}

/*Functions from 2TheBasics.h file*/

void _initializer_list()
{
    int i1 = 7.2;   //i1 becomes 7
    //int i2 {7.2};   //error/warning: floating-point to integer conversion
    std::complex<double> z = 1;  //a complex number <1,0> ; direct class initialization
}

void _constness()
{
    const int dmv = 17;
    int var {17};
    constexpr double max1 = 1.4*square(dmv);    //OK, only if square(dmv) is a constant expression
    //constexpr double max2 = 1.4*square(var);    //error: var is not const
    const double max3 = 1.4*square(var);        //may be evaluated at run-time
    std::vector<double> v {1.2, 2.3, 3.4};
    const double s1 = sum(v);       //evaluated at run time
    //constexpr double s2 = sum(v);   //error: sum(v) not a constant expression
}

void _vectors()
{
    Vector v(5);    //defined a vector of size 5
    std::cout << "Enter 5 values of vector\n";
    for (int i = 0; i < v.size(); ++i)
        std::cin >> v[i];
    //v[v.size()] = 7.2;      //will surely throw an exception
}

void _static_assertions()
{
    //static_assert(A,S) prints S as compiler message if A is not true.
    //It is a compile-time mechanism.
    static_assert(sizeof(int) >= 4, "integer size too small!");
}

void run_TheBasics()
{
    _initializer_list();
    _constness();
    try {
        _vectors();
    } catch (std::out_of_range& oore) {
        std::cerr << oore.what() << '\n';
    } catch (std::length_error& le) {
        std::cerr << le.what() << '\n';
    } catch (std::bad_alloc& bae) {
        std::cerr << bae.what() << '\n';
    }
    _static_assertions();
}
