/**
* Author: Sahil (email: enthusiast.sahil@yahoo.in)
* Date:
* Description: This file contains all programming examples of 3rd Chapter.
*/

#include <stdexcept>    //contains class runtime_error
#include <iostream>
#include <vector>
#include <memory>
#include <initializer_list>
#include <string>
#include <thread>
#include "3AbstractionMechanisms.h"

class DivideByEmptyComplexError : public std::runtime_error
{
public:
    DivideByEmptyComplexError()
    :std::runtime_error("Division by empty Complex Number is not defined") {}
};


Complex& Complex::operator*=(Complex z)  //Section 3.2.1.1
{
    re = re*z.real() - im*z.imag();
    im = re*z.imag() + im*z.real();
    return *this;
}

Complex& Complex::operator/=(Complex z)  //Section 3.2.1.1
{
    re = (re*z.real() + im*z.imag())/(z.real()*z.real() + z.imag()*z.imag());
    im = (im*z.real() - re*z.imag())/(z.real()*z.real() + z.imag()*z.imag());
    return *this;
}

//other useful functions which make use of the Complex class
Complex operator+(Complex a, Complex b)
{
    return a += b;
}

Complex operator-(Complex a, Complex b)
{
    return a -= b;
}

Complex operator-(Complex z)        //unary minus
{
    return {-z.real(), -z.imag()};
    /*
    Let expression <-z.real(), -z.imag()> be denoted by E.
    Type Complex is copy-initializable from E (since constructors are non-explicit).
    Hence, E is implicitly-convertible to type Complex.
    So, the compiler, instead, calls the matching constructor call, here, Complex(double, double).
    The temporary, thus generated, is copy-elided due to pass-by-value semantics.
    Note:- In copy-elision, only constructers (copy and move) are evaded, not the destructors!
      */
}

Complex operator*(Complex a, Complex b)
{
    return a *= b;
}

Complex operator/(Complex a, Complex b)
{
    if(b.real() == 0 && b.imag() == 0)
        throw DivideByEmptyComplexError{};
    return a /= b;
}

std::ostream& operator<<(std::ostream& out, Complex a)
{
    out << "<" << a.real() << ", " << a.imag() << ">";
    return out;
}

void _complex()
{
    Complex a{2.3};     //construct<2.3,0> from 2.3 (direct initialization)
    Complex b{a/3};     //note that a is passed by copy
    /*  a could not be copy-elided because operator / retuns a, which is "also a function parameter"  */
    std::cout << "b initialized\n";
    std::cout << a.real() << '\t' << a.imag() << '\n';  //to prove that a was passed by copy
    Complex c {-a};     //incurs two destructors
    std::cout << "XXXXXXXXXXXX\n";
    try {
        Complex d = 1/Complex{};
    } catch(DivideByEmptyComplexError& dbece) {
        std::cerr << dbece.what() << '\n';
    }
  /*
    Expression a/3 is implicitly-converted to a/Complex{3}
    by constructor call Complex(double), which is required to be non-explicit.
    Result of a/3 generates a temporary Complex, which copy-initializes b.
    3 Destructors are called after before b is initialized :-
    > The object created through Complex{3}
    > The object a copied into operator /
    > The temporary obtained after operation a/3

    One Constructor is called for constructing from 3.
    Object Complex{3} is copy-elided, not passed by copy!
    The return parameter a in function / is copy elided into the temporary.
    In the last phase, Temporary -> b, temporary T is copy-elided into b, that is,
    T is constructed directly in the storage of b, because b is copy-initializable from T.
    This is known as RVO (Return Value Optimization).
  */
}

Smiley::Smiley(int sz)
    :Circle{sz}
{
    //initialize the representation (components) of Smiley
    add_eye(shape_resource {new Circle(sz/5)});
    add_eye(shape_resource {new Circle(sz/5)});
    set_mouth(shape_resource {new Square(sz/3)});
}

void Smiley::draw() const
{
    std::cout << "Smiley start drawing\n";
    Circle::draw();     //calls base class's method
    for (auto& e : eyes)
        e->draw();
    if(mouth != nullptr)
    mouth->draw();
    std::cout << "Smiley ends drawn\n";
}

void Smiley::rotate(int angle)
{
    for (auto& e : eyes)
        e->rotate(angle);
    if(mouth != nullptr)
        mouth->rotate(angle);
}

//function for rotate
void rotate_all(std::vector<std::unique_ptr<Shape>>& v, int angle)
{
    for (auto& shape : v)
        shape->rotate(angle);
}

//function for draw
void draw_all(std::vector<std::unique_ptr<Shape>>& v)
{
    for (auto& shape : v)
        shape->draw();
}

//single function for any kind of generic operation on the shapes
template <typename Con, typename Oper>
void for_all(const Con& c, Oper op)
{
    for (auto& shape : c)
        op(shape);
}

void _shape_hierarchy()
{
    std::vector<std::unique_ptr<Shape>> v;
    int size = 15;
    std::unique_ptr<Smiley> sm1{new Smiley{size}};
    size = 30;
    std::unique_ptr<Smiley> sm2{new Smiley{size}};

    v.push_back(std::move(sm1));
    v.push_back(std::move(sm2));

    //draw_all(v);
    //rotate_all(v, 45);
    //Instead of writing many functions which do specific operation to all members,
    //we can instead write a generic function for_all().
    for_all(v, [](const std::unique_ptr<Shape>& s){s->draw();});
    for_all(v, [](const std::unique_ptr<Shape>& s){s->rotate(45);});
}//all Shapes implicitly destroyed.

template <typename T>
GenericVector<T>::GenericVector(int s)
{
    if(s <= 0)
        throw std::length_error{"In GenericVector::GenericVector(int), length can not be non-positive"};
    sz = s;
    std::cout << "GenericVector(int) - Acquiring space\n";
    elem = new T[sz];
}

template <typename T>
GenericVector<T>::GenericVector(std::initializer_list<T> lst)
    :sz{lst.size()}, elem{new T[sz]}
{
    std::cout << "GenericVector(initializer_list) - Acquiring resources\n";
    std::copy(lst.begin(), lst.end(), elem);
}

//copy semantics
template <typename T>
GenericVector<T>::GenericVector(GenericVector const& a)   //copy constructor
    :sz{a.size()}, elem{new T[sz]}
{
    for (int i = 0; i != sz; ++i)
        elem[i] = a[i];
    std::cout << "GenericVector - copy constructor\n";
}

template <typename T>
GenericVector<T>& GenericVector<T>::operator=(GenericVector const& a)  //copy assignment
{
    std::cout << "GenericVector - copy assignment\n";
    delete [] elem;     //delete old elements first
    sz = a.size();
    elem = new T[sz];
    for (int i = 0; i != sz; ++i)
        elem[i] = a[i];
    return *this;
}

//move semantics
template <typename T>
GenericVector<T>::GenericVector(GenericVector&& a)    //move constructor
    :sz{a.sz}, elem{a.elem}     //private members of foreign class are accessible in a constructor
{
    a.elem = nullptr;   //grab elements from a and leave it in a consistent state
    a.sz = 0;
    std::cout << "GenericVector - move constructor\n";
}

template <typename T>
GenericVector<T>& GenericVector<T>::operator=(GenericVector&& a)  //move assignment
{
    std::cout << "GenericVector - move assignment\n";
    delete [] elem;
    sz = a.sz;
    elem = a.elem;
    a.elem = nullptr;
    a.sz = 0;
    return *this;
}

template <typename T>
T& GenericVector<T>::operator[](size_t index)
{
    if(index < 0 || index >= size())
        throw std::out_of_range{"In GenericVector::operator[](int), illegal index provided"};
    return elem[index];
}

template <typename T>
T const& GenericVector<T>::operator[](size_t index) const
{
    if(index < 0 || index >= size())
        throw std::out_of_range{"In GenericVector::operator[](int) const, illegal index provided"};
    return elem[index];
}

//functions not part of the GenericVector class specification
class GenericVector_size_mismatch : public std::runtime_error    //custom error class for runtime error
{
public:
    GenericVector_size_mismatch(std::string error_report)
        :std::runtime_error(error_report) {}
};

template <typename T>
GenericVector<T> operator+(const GenericVector<T>& a, const GenericVector<T>& b)
{
    if(a.size() != b.size())
        throw GenericVector_size_mismatch{"in operator+(Vec a, Vec b), sizes of a & b differ"};
    GenericVector<T> res(a.size());
    for (unsigned int i = 0; i != res.size(); ++i)
        res[i] = a[i] + b[i];
    return res;     //move assignment called
}

template <typename T>
void f(const GenericVector<T>& x, const GenericVector<T>& y)
{
    //GenericVector<T> r;     //push_back not defined yet, so size is required
    GenericVector<T> r{};
    r = x + y;  //copy-elision, copy and move constructors avoided
}

template<typename T>
void g(const GenericVector<T>& a)
{
    for(auto& e : a)        //range-for possible because we have defined begin() and end()
        std::cout << e << "\n";   //ostream operator << required to be already defined for T
}

void _generic_vector()
{
    GenericVector<double> v1 {2.3, 3.4, 4.5};
    GenericVector<double> v2 {1.2, 2.3, 3.4};
    std::cout << "control goes into f()\n";
    try {
        f(v1, v2);
    } catch (GenericVector_size_mismatch& gsme) {
        std::cerr << gsme.what() << std::endl;
    }
    GenericVector<std::string> v3(2);
    v3[0] = "Sahil";
    v3[1] = "Kakkar";
    g(v3);
}

void heartbeat()    //heartbeat is a concurrent activity
{
    for(int i = 0; i != 10; ++i)
        std::cout << "!!!heart is beating!!!\n";
}

std::vector<std::thread> my_threads;    //contains many such concurrent activities

GenericVector<double> init(int n)
{
    std::thread t{heartbeat};   //run heartbeat concurrently (on its own thread)
    //comment the following line to attain concurrency of the two threads
    t.join();     //init() waits for completion of t
    my_threads.push_back(std::move(t));   //can't copy a thread

    //other initializations;
    GenericVector<double> vec(n);
    for (auto& e : vec)
        e = 777;
    return vec;     //move vec out of init
}

void _resource_management()
{
/*  Use -pthread in linker options  */
    auto v = init(1000);
/*In much the same way as new and delete disappear into the constructors and destructors,
we can make pointers disappear into resource handles (like here, vectors & threads).
*/
}

template <typename Container, typename Elem>
Elem sum(Container& c, Elem e)
{
    for (auto& x : c)
        e += x;     //operator += must be valid on e
    return e;
}

void user(GenericVector<int>& vi, GenericVector<double>& vd, GenericVector<Complex>& vc, GenericVector<std::string>& vs)
{
    std::cout << sum(vi, 0) << '\n';
    std::cout << sum(vd, 0.0) << '\n';
    std::cout << sum(vc, Complex{}) << '\n';
    std::cout << sum(vs, std::string{}) << '\n';
}

void _generic_sum()
{
    GenericVector<int> vi {1, 2, 3, 4};
    GenericVector<double> vd {1.2, 2.3, 3.4, 4.5};
    GenericVector<Complex> vc {{0.1, 1.2}, {0.3, 2.7}, {3.14, 0.6}};
    GenericVector<std::string> vs {"Sahil", "Parveen", "Kakkar"};
    user(vi, vd, vc, vs);
}

template <typename T>
class Less_Than
{
public:
    Less_Than(const T& value)
        :val{value} {}
    bool operator()(const T& x) const
    {
        return x < val;
    }
private:
    const T val;    //value to compare against
};

template <typename Container, typename Predicate>
unsigned int count(Container& c, Predicate pred)
{
    unsigned int cnt = 0;
    for (auto& x : c)
        if(pred(x))
            ++cnt;
    return cnt;
}

void check(const GenericVector<int>& vi, int i, const GenericVector<std::string>& vs, const std::string& s)
{
    std::cout << "Number of integers less than " << i << " = "
    << count(vi, Less_Than<int> {i}) << '\n';

    std::cout << "Number of strings less than " << s << " = "
    << count(vs, Less_Than<std::string> {s}) << '\n';
}

void another_check(const GenericVector<int>& vi, int i, const GenericVector<std::string>& vs, const std::string& s)
{
    //same operations using lambdas
    std::cout << "Number of integers less than " << i << " = "
    << count(vi, [&](int a){return a < i;}) << '\n';

    std::cout << "Number of strings less than " << s << " = "
    << count(vs, [&](std::string a){return a < s;}) << '\n';
}

void _function_objects()
{
    GenericVector<int> vi {3, 5, 2, 6, 13, 9};
    GenericVector<std::string> vs {"hello", "world", "arun", "singh"};
    check(vi, 7, vs, std::string {"kepler"});
    another_check(vi, 7, vs, std::string {"kepler"});
}

template <typename T>
void func(const T& t)
{
    std::cout << t << '\n';
}

void variadic() {}  //when recursively goes out of arguments, do nothing

template <typename H, typename... T>
void variadic(const H& head, T... tail)
{
    func(head);     //handle each argument in func()
    variadic(tail...);
}

void _variadic_templates()
{
    variadic(1, 3.14, std::string{"cplusplus11"}, Complex{1.2, 3.4});
}

void run_AbstractionMechanisms()
{
    std::cout << "_complex() starts\n";
    _complex();
    std::cout << "_complex() ends\n\n";

    std::cout << "_shape_hierarchy() starts\n";
    _shape_hierarchy();
    std::cout << "_shape_hierarchy() ends\n\n";

    std::cout << "_generic_vector() starts\n";
    _generic_vector();
    std::cout << "_generic_vector() ends\n\n";

    std::cout << "_resource_management() starts\n";
    _resource_management();
    std::cout << "_resource_management() ends\n\n";

    std::cout << "_generic_sum() starts\n";
    _generic_sum();
    std::cout << "_generic_sum() ends\n\n";

    std::cout << "_function_objects() starts\n";
    _function_objects();
    std::cout << "_function_objects() ends\n\n";

    std::cout << "_variadic_templates() starts\n";
    _variadic_templates();
    std::cout << "_variadic_templates() ends\n\n";
}
