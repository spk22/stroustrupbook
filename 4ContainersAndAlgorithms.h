/**
 * Author: Sahil Kakkar <enthusiast.sahil@yahoo.in>
 * Date:
 * Description: This file contains all programming examples of 4th Chapter.
 * A string is mutable by default!
 */

#ifndef CONTAINERS_AND_ALGORITHMS
#define CONTAINERS_AND_ALGORITHMS

void run_ContainersAndAlgorithms();

struct Entry    //represents entries in a telephone book
{
    std::string name;
    int house_number;
};

void _strings();
void _streamio();
void _containers();

#endif // CONTAINERS_AND_ALGORITHMS

