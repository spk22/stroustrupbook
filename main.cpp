/**
* Author: Sahil Kakkar <enthusiast.sahil@yahoo.in>
* Description: This project contains all programming examples of Stroustrup's book TCPL 4th Edition
*
* > Some examples of resource handles are :
* vector, string, thread, mutex, unique_ptr, fstream and regex.
*
* > Except for the new, delete, typeid, dynamic_cast, and throw operators, and try-block,
* individual C++ expressions and statements need no run-time support.
*
*/

#include <iostream>
#include <fstream>
#include <cstring>
#include <cstdlib>

/*Header files for Book Chapters*/
#include "2TheBasics.h"
#include "3AbstractionMechanisms.h"
#include "4ContainersAndAlgorithms.h"

using namespace std;

void help()
{
    ifstream fin("D:\\DeveloperArena\\gitrepo\\StroustrupBook\\help.txt");   //open file for reading
    string s;
    if(!fin)
        cout << "Can't open/locate help.txt. Check file permissions\n";

    while(getline(fin, s))
        cout << s << '\n';
}

int main(int argc, char* argv[])
{
    if (argc < 2)
        cout << "Enter argument -h for options\n";
    else if(!strcmp(argv[1], "-h") || !strcmp(argv[1], "-H") || !strcmp(argv[1], "--help") ||
            !strcmp(argv[1], "--Help") || !(strcmp(argv[1], "-help")) || !(strcmp(argv[1], "-Help")))
        help();
    else
    {
        int option = atoi(argv[1]);
        switch(option)
        {
        case 2:
            run_TheBasics();
            break;
        case 3:
            run_AbstractionMechanisms();
            break;
        case 4:
            run_ContainersAndAlgorithms();
            break;
        default:
            cout << "Incorrect Chapter number\n";
        }   //switch ends
    }
    return 0;
}
