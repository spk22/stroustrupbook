/**
 * Author: Sahil Kakkar <enthusiast.sahil@yahoo.in>
 * Date:
 * Description: This file contains all programming examples of 3rd Chapter.
 */

#ifndef ABSTRACTION_MECHANISMS_H
#define ABSTRACTION_MECHANISMS_H

#include <iostream>
#include <memory>
#include <vector>
#include <initializer_list>
#include <cstddef>

void run_AbstractionMechanisms();

/*Functions defined in a class are inline by default*/

class Complex   //concrete class, Section 3.2.1.1
{
public:
    Complex()               //constructs <0,0>
        :re{0}, im{0}
    {
        std::cout << "Complex constructed from no scalars\n";
    }
    Complex(double r)       //constructs <r,0>
        :re{r}, im{0}
    {
        std::cout << "Complex constructed from one scalar\n";
    }
    Complex(double r, double i)     //constructs <r,i>
        :re{r}, im{i}
    {
        std::cout << "Complex constructed from two scalars\n";
    }

    double real() const     //const implies representation is untouched in the definition
    {
        return re;
    }

    double imag() const
    {
        return im;
    }

    void real(double d)
    {
        re = d;
    }

    void imag(double d)
    {
        im = d;
    }

    Complex& operator+=(Complex z)      //argument z passed by value, hence is copied (not moved).
    {
        re += z.real();
        im += z.imag();
        return *this;
    }

    Complex& operator-=(Complex z)      //return-type is lvalue, to make the result assignable.
    {
        re -= z.real();
        im -= z.imag();
        return *this;
    }

    Complex& operator*=(Complex z);     //implementation defined outside the class specification
    Complex& operator/=(Complex z);     //implementation defined outside the class specification

    ~Complex()
    {
        std::cout << "~Complex() destructed\n";
    }

private:
    double re, im;      //representation is part of the class interface
};  //class Complex ends

class Shape    //abstract base, Section 3.2.4
{
public:
    Shape() {}
    //no copy
    Shape(const Shape&) = delete;
    Shape& operator=(const Shape&) = delete;
    //no move
    Shape(Shape&&) = delete;
    Shape& operator=(const Shape&&) = delete;

    //virtual functions
    virtual void draw() const = 0;
    virtual void rotate(int angle) = 0;

    virtual ~Shape() {}
};

class Circle : public Shape    //concrete class
{
public:
    Circle(int r = 10)
        :rad{r} {}
    void draw() const {std::cout << "Circle of radius " << rad << " drawn\n";}
    void rotate(int angle)
    {
        std::cout << "Circle rotated by " << angle << " degrees\n";
    }
    //~Circle();      //not required to be defined, as base Shape destructor is not purely virtual
private:
    int rad;
};

class Square : public Shape    //concrete class
{
public:
    Square(int s = 5)
        :side{s} {}
    void draw() const {std::cout << "Square of radius " << side << " drawn\n";}
    void rotate(int angle)
    {
        std::cout << "Square rotated by " << angle << " degrees\n";
    }
    //~Square();
private:
    int side;
};

class Smiley : public Circle    //concrete class, a resource handle
{
    using shape_resource = std::unique_ptr<Shape>;    //class will handle a Shape derivative
public:
    Smiley(int);
    void draw() const;   //construction of smiley does not imply Smiley drawn!
    //draw function not compulsary to be defined by Smiley, but what else can you do with a Smiley :P
    void rotate(int angle);     //rotate smiley along with its eyes and mouth
    //~Smiley();      //no need to handle resources manually, unique_ptr does it for you

private:
    std::vector<shape_resource> eyes;     //usually 2 eyes of any Shape
    shape_resource mouth;     //a mouth of any Shape

    /*  internal utilities  */
    void add_eye(shape_resource s)
    {
        eyes.push_back(std::move(s));
    }
    void set_mouth(shape_resource m)
    {
        mouth = std::move(m);
    }
};

template <typename T>
class GenericVector     //Section 3.3 & 3.4.1
{
public:
    using value_type = T;
    GenericVector(int);
    GenericVector(std::initializer_list<T>);

    //copy semantics, only for tutorial purpose. Use ready-made resource handles like vector, thread, mutex etc
    GenericVector(GenericVector const&);
    GenericVector& operator=(GenericVector const&);

    //move semantics, only for tutorial purpose. Use ready-made resource handles like vector, thread, mutex etc
    GenericVector(GenericVector&&);
    GenericVector& operator=(GenericVector&&);

    T& operator[](size_t);    //return reference of v[i] (not copy), as lvalue (assignable, mutable)
    T const& operator[](size_t) const; //return reference of v[i] (not copy), as lvalue (non-assignable, immutable)
    size_t size() const    //good practice to declare non-modifying functions as const
    {
        return sz;
    }

    //to support range-for loop, define begin() and end()
    T* begin()
    {
        return &elem[0];   //pointer to first element
    }
    T const* begin() const
    {
        return &elem[0];
    }
    T* end()
    {
        return &elem[sz];  //pointer to one past the last element
    }
    T const* end() const
    {
        return &elem[sz];
    }

    ~GenericVector() {std::cout << "~GenericVector - releasing resources\n"; delete [] elem;}
private:
    size_t sz;
    T* elem;
};

void _complex();
void _shape_hierarchy();
void _generic_vector();
void _resource_management();
void _generic_sum();
void _function_objects();
void _variadic_templates();

#endif // ABSTRACTION_MECHANISMS_H
